const path = require("path");
const HDWalletProvider = require("@truffle/hdwallet-provider");
var mnemonic = "orange apple banana ... ";

const privateKeys = [
  "095a9ed8feb301819deb8271bb93ab88532d86d41a2760e9ea8cceb745b02197",
  "b4e780e5c91014dd34f2f2f1bb225cc3c4265f1fce82394aeb4821d921fe4e07",
];

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    develop: {
      port: 8545
    },
    rinkeby: {
      provider: function () {
        return new HDWalletProvider(privateKeys, "wss://rinkeby.infura.io/ws/v3/8400efcb26264bab9baa73b0227c4b74");
      },
      network_id: 4,
      gas: 4500000,
      gasPrice: 10000000000,
    }
  },
  compilers: {
    solc: {
      version: "^0.8.6"
    }
  }
};
